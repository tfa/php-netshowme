<?php 
if(!function_exists("getClientIpAddress")){
  
    function getClientIpAddress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
		elseif (!empty($_SERVER['HTTP_X_FORWARDED']))
        {
          $ip = $_SERVER['HTTP_X_FORWARDED'];
        }
		 elseif (!empty($_SERVER['HTTP_FORWARDED_FOR']))
        {
          $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        }
		elseif (!empty($_SERVER['HTTP_FORWARDED']))
        {
          $ip = $_SERVER['HTTP_FORWARDED'];
        }
        else
        {
          $ip = $_SERVER['REMOTE_ADDR'];
        }
		
        return ($ip == '::1' || $ip == 'localhost') ? '127.0.0.1' : $ip;
    }
}
?>