<?php $local_session = \Config\Services::session();  ?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo site_url();?>/public/assets/css/style.css" rel="stylesheet">
<link href="<?php echo site_url();?>/public/assets/css/upload.css" rel="stylesheet">
<?php echo view('header')?>
<div class="container-fluid h-100">
  <div class="row justify-content-center align-items-center">
		<div class="container mt-4">
			<h1>NetShow.me  - Send Message Form</h1>
			<?php if($local_session->get('success_message')) { ?>
				<div class="alert alert-success"><?php echo $local_session->get('success_message'); ?></div> 
			<?php } else if($local_session->get('error_message')){?>
				<div class="alert alert-danger"><?php echo $local_session->get('error_message'); ?></div> 
			<?php } ?>
			
			<?php if($local_session->get('mail_sent_message')) { ?>
				<div class="alert alert-info"><?php echo $local_session->get('mail_sent_message'); ?></div> 
			<?php } ?>
			<form name="frmSend" method="POST" action="SendController/insert" enctype='multipart/form-data'>
			 <div class="form-group">
				<label for="nomeTxt">Nome </label>
				<input type="text" class="form-control" id="nomeTxt" placeholder="Ex: Fulano de Tal" required name="name">
			  </div>
			   <div class="form-group">
				<label for="emailTxt">Email </label>
				<input type="email" class="form-control" id="emailTxt" placeholder="name@example.com" required name="email">
			  </div>
			  <div class="form-group">
				<label for="telefoneTxt">Telefone </label>
				<input type="phone" class="form-control" id="telefoneTxt" placeholder="(xx) xxxxx-xxxxx" required name="phone"   onkeypress="mask(this, mphone);" onblur="mask(this, mphone);" >
			  </div>
			  <div class="form-group">
				<label for="mensagemTxt">Mensagem</label>
				<textarea class="form-control" id="mensagemTxt" rows="3" required name="msg"></textarea>
				</div>
              <div class="form-group files">
                <label>Arquivo <small><strong>(Tamanho MAX de arquivo: 500KB.  Extensões Permitidas : pdf, doc, docx, odt, txt) </strong></small> </label>
                <input type="file" class="form-control" id="arquivos" name="file">
              </div>
			  <div class="form-group">
				<input type="submit" class="btn btn-primary btn-block btn-lg" value="Enviar">
			  </div>
		</div>
	</div>
</div>
<script src="<?php echo site_url();?>/public/assets/js/functions.js"></script>
<?php $local_session->destroy();?>