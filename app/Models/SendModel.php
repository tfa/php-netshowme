<?php

namespace App\Models;

use CodeIgniter\Model;

class SendModel extends Model
{
    protected $table      = 'informations';
    protected $primaryKey = 'id_information';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['name', 'email', 'phone', 'msg', 'file', 'ip_address'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
?>