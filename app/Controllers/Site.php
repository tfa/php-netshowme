<?php

namespace App\Controllers;

class Site extends BaseController
{
  public function get_client_ip_address()
  {
    return getClientIpAddress();
  }
}
?>