<?php

namespace App\Controllers;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require  ROOTPATH.'app\Libraries\PHPMailer/src/Exception.php';
require  ROOTPATH.'app\Libraries\PHPMailer/src/PHPMailer.php';
require  ROOTPATH.'app\Libraries\PHPMailer/src/SMTP.php';

class SendController extends BaseController
{
	public function index()
	{
		return view('formulario');
	}
	public function sendMail($from, $to, $msg)
	{
		$mail = new PHPMailer(true);
		try
		{	
			$mail->isSMTP();       
			$mail->SMTPAuth = true;
			$mail->Username   = 'XXXXXX@XXXXXX.com.br';
			$mail->Password   = 'XXXXXXXXXXXX';
			
			$mail->SMTPSecure = 'ssl';
			$mail->SMTPOptions = array(
				'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
				)
			);
			
			$mail->Host = 'mail.tfarte.com.br';
			$mail->Port = 465;
			$mail->setFrom($from, 'TFARTE');
			$mail->addAddress($to, 'TFArte Comercial');
			$mail->isHTML(true);
			$mail->Subject = 'ENVIO DE EMAIL';
			$mail->Body    = $msg;
			$mail->AltBody = 'Este é o cortpo da mensagem para clientes de e-mail que não reconhecem HTML';
			
			$mail->send();
			
			$session->set('success_message', 'Mensagem enviada com sucesso.');
			return redirect()->to('/');
		}
		catch (Exception $e)
		{
			echo "A mensagem nao pode ser enviada. Mailer Error: {$mail->ErrorInfo}";
		}
	}
	
	public function insert()
	{		
		/*Start session*/
		$session = \Config\Services::session();
		
		/*Site ipaddress function instantiate*/
		$site = new Site();
		
		/*Get Ip Client*/
		$ipAddress = $site->get_client_ip_address();
		
		/*Validation File*/
		$validation = \Config\Services::validation();
		$validation->setRule('file', 'File', 'uploaded[file]|max_size[file,500]|ext_in[file,doc,txt,odt,docx,pdf]');
		$validation->withRequest($this->request)->run();
		$errors = $validation->getErrors();
		
		/*Check Errors*/
		if(count($errors) > 0){
			$session->set('error_message', 'Erro no arquivo. por favor verifique.');
			return redirect()->to('/');
		}
		
		$file = $this->request->getFile('file');
		
		/*Check File*/
		if($file)
		{
			/*Check if file has not been moved yet*/
			if ($file->isValid() && ! $file->hasMoved())
			{
				/*Get FileName*/
			   $fileName = $file->getName(); 
			}
		}
		
		
		/*POST Fields*/
		$insertFields = array(
			'name' 		=> $this->request->getVar('name'), 
			'email' 	=> $this->request->getVar('email'),
			'phone' 	=> $this->request->getVar('phone'),
			'msg' 		=> $this->request->getVar('msg'),
			'file'		=> $fileName,
			'ip_address' => $ipAddress
		);
		
		/*Save data into database*/
		$sendModel = new \App\Models\SendModel();
		$insertResponse = $sendModel->save($insertFields);
		
		/*Check SAVE*/		
		if($insertResponse) {
			
			$file->move(WRITEPATH.'uploads', $fileName); //MOVE file to UPLOAD folder
			
			/*Success Message*/				
			$session->set('success_message', 'Dados enviados com sucesso.');
			
			/*Send Email*/
			$this->sendMail('contato@tfarte.com.br', $insertFields['email'], $insertFields['msg']);
		}else{
			/*Error Message*/
			$session->set('error_message', 'Não foi possivel o envio dos dados.');
		}
			
		/*Redirecting...*/
		return redirect()->to('/');
	}
}
