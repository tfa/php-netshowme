<?php

namespace CodeIgniter;

use CodeIgniter\Test\ControllerTestTrait;
use CodeIgniter\Test\CIUnitTestCase;
use CodeIgniter\Test\DatabaseTestTrait;

class TestSendController extends CIUnitTestCase
{
    use ControllerTestTrait, DatabaseTestTrait;

    public function testInsert()
    {
        $result = $this->withURI('http://localhost/SendController/insert')
                       ->controller(\App\Controllers\SendController::class)
                       ->execute('insert');

        $this->assertTrue($result->isOK());
    }
	
	public function testSendMail()
	{
		$result = $this->withURI('http://localhost/SendController/sendMail')
                       ->controller(\App\Controllers\SendController::class)
                       ->execute('sendMail');

        $this->assertTrue($result->isOK());
	}
}
?>