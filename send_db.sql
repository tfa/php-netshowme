-- MySQL dump 10.13  Distrib 8.0.14, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: send_db
-- ------------------------------------------------------
-- Server version	8.0.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `informations` (
  `id_information` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `msg` text,
  `file` text,
  `ip_address` varchar(45) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_information`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'teste','teste@teste.com.br','2199834567','mfnsafbnfasbnfb',NULL,NULL,'2021-05-18 00:08:48'),(2,'teste','teste@teste.com.br','2199834567','sdnbfsdjbfsdjbf','xyz',NULL,'2021-05-18 00:08:48'),(3,'','','','','xyz',NULL,'2021-05-18 00:08:48'),(4,'','','','','xyz',NULL,'2021-05-18 00:08:48'),(5,'','','','','xyz',NULL,'2021-05-18 00:08:48'),(6,'Thalles','thallesfaria@hotmail.com','(21) 96973-9918','qwoifqwrqwfhqwfiohqw','xyz',NULL,'2021-05-18 00:08:48'),(7,'THALLES','teste@teste.com','21111111111111','lfksdnjkfnd','xyz',NULL,'2021-05-18 00:08:48'),(8,'asfjkasjf','jnfjasnf@teste.com','133232','nef wnefwefwefnmwe','xyz',NULL,'2021-05-18 00:08:48'),(9,'asfjkasjf','jnfjasnf@teste.com','133232','nef wnefwefwefnmwe','xyz',NULL,'2021-05-18 00:08:48'),(10,'asfjkasjf','jnfjasnf@teste.com','133232','nef wnefwefwefnmwe','xyz',NULL,'2021-05-18 00:08:48'),(11,'asfjkasjf','jnfjasnf@teste.com','133232','nef wnefwefwefnmwe','xyz',NULL,'2021-05-18 00:08:48'),(12,'asfjkasjf','jnfjasnf@teste.com','133232','nef wnefwefwefnmwe','xyz',NULL,'2021-05-18 00:08:48'),(13,'thalles','thallesfaria@hotmail.com','21994433462','TESTANDO FORM','xyz',NULL,'2021-05-18 00:08:48'),(14,'thalles','thallesfaria@hotmail.com','21994433462','TESTANDO FORM','xyz',NULL,'2021-05-18 00:08:48'),(15,'MAIS UMA VEz','vez@vez.com','21994433462','TESTANDO A ULTIMA VEZ','xyz',NULL,'2021-05-18 00:08:48'),(16,'MAIS UMA VEz','vez@vez.com','21994433462','TESTANDO A ULTIMA VEZ','xyz',NULL,'2021-05-18 00:08:48'),(17,'MAIS UMA VEz','vez@vez.com','21994433462','TESTANDO A ULTIMA VEZ','xyz',NULL,'2021-05-18 00:08:48'),(18,'MAIS UMA VEz','vez@vez.com','21994433462','TESTANDO A ULTIMA VEZ','xyz',NULL,'2021-05-18 00:08:48'),(19,'TESTADOR','teste@teste.com','219998765467','TESTANDO IP','xyz',NULL,'2021-05-18 00:09:27'),(20,'Teste2','teste2@teste.com','21994433462','TESTANDO MAIS UMA VEZ','xyz',NULL,'2021-05-18 01:26:41'),(21,'Teste2','teste2@teste.com','21994433462','TESTANDO MAIS UMA VEZ','xyz',NULL,'2021-05-18 01:30:41'),(22,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg',NULL,'2021-05-18 01:33:32'),(23,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg',NULL,'2021-05-18 01:39:11'),(24,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg',NULL,'2021-05-18 01:40:09'),(25,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg',NULL,'2021-05-18 01:40:24'),(26,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg',NULL,'2021-05-18 01:40:45'),(27,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg',NULL,'2021-05-18 01:41:58'),(28,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:42:52'),(29,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:44:47'),(30,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','localhost','2021-05-18 01:45:04'),(31,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:45:12'),(32,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:48:24'),(33,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:50:02'),(34,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:51:17'),(35,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:51:41'),(36,'Teste 1','teste@teste.com','21994433462','TESTE D NOVO','tree1.jpg','127.0.0.1','2021-05-18 01:52:46'),(37,'teste session','session@testesession.com','218474574744','ifweufweifh','acai.jpg','127.0.0.1','2021-05-18 01:53:07'),(38,'jqfqwjfh','jiqnfjw@fujfd.com','327327832787238','jfwenfjwenfjwenfjkwenfjkn','boot1.png','127.0.0.1','2021-05-18 01:55:06'),(39,'qjkfnqwfjkwqjkfbn','qjfnwejknf@fjwebfjw.com','378783738','jfwebjkfbwejkf','acai.png','127.0.0.1','2021-05-18 01:55:50'),(40,'qfjkwbejfwbejkf','kjwebfwjkebf@jfbdjf.com','782378237823','jhbfwejkbfwejkfbwe','aperol.jpg','127.0.0.1','2021-05-18 01:56:30'),(41,'TESTADOR','testador123@teste.com','2132423423423','teste','arm2.jpg','127.0.0.1','2021-05-18 02:00:31'),(42,'asfhabfhab','jqfjkasbf@jsdbhfjd.com','(21) 9876-5678','dknfwebfwejbfwed,zxmnxv TELEFONE','chef_fish.jpg','127.0.0.1','2021-05-18 02:02:46'),(43,'teste','envio@teste.com','(21) 7364-5363','tetetetegsgdbcnc','aperol.jpg','127.0.0.1','2021-05-18 02:29:19'),(44,'weuifweufgweui','iwuehfi@fuhwefh.com','(22) 32323-2323','wejfbwejfbwejfbwejfb',NULL,'127.0.0.1','2021-05-18 02:48:40'),(45,'sjkfbwejbf','kjwnfwk@jfewfj.com','(23) 33232-2323','wejkfbwejkbfwejkf','acai.png','127.0.0.1','2021-05-18 02:48:55'),(46,'kfjwbnjkfweb','jfkwnejfk@jfe.com','(21) 2332-3232','fmwefnwmefnwmefn','aperol.jpg','127.0.0.1','2021-05-18 02:50:15'),(47,'sdfkjsdnjk','kwefnjkf@jfwejf.com','(23) 23323-2232','wejfiwejfwebbfwj','aperol.jpg','127.0.0.1','2021-05-18 02:50:29'),(48,'sdkfsdnfjhl','jkwnfwe@jfwdnf.com','(23) 22332-3232','jweifnwejfnwejkfn','acai.jpg','127.0.0.1','2021-05-18 02:51:22'),(49,',vmxcvnxcmvnxcmvx','ksskdlf@kdkdkdk.com','(33) 73237-7373','jnfwbenjbnfwbnfbwef','alura.txt','127.0.0.1','2021-05-18 02:54:54'),(50,'teste mail','contato@tfarte.com.br','(21) 33344-3434','wefoijwefiojweifowjeofjwfijw','alura.txt','127.0.0.1','2021-05-18 18:00:18'),(51,'teste mail','contato@tfarte.com.br','(21) 33344-3434','wefoijwefiojweifowjeofjwfijw','alura.txt','127.0.0.1','2021-05-18 18:00:47'),(52,'teste mail','contato@tfarte.com.br','(21) 33344-3434','wefoijwefiojweifowjeofjwfijw','alura.txt','127.0.0.1','2021-05-18 18:01:42'),(53,'TESTE MAIL','contato@tfarte.com.br','(22) 58459-4589','oioioioioioio FUNCIONOU!!!!','atlasian.txt','127.0.0.1','2021-05-18 18:07:08'),(54,'asbjasbfajksf','jnfjksdn@jfdsnfj.com','(38) 27327-3272','jwenfbwejbfwejkbfwejkfbwejk','alura.txt','127.0.0.1','2021-05-18 19:26:31'),(55,'kfjnwejkfnwejkfn','jkwenfjkwef@jfwnfj.com','(37) 37327-7237','jwebffjwebjkfwebjkfwef','win10_serial.txt','127.0.0.1','2021-05-18 21:25:46'),(56,'jifnwejfbwejfb','kwjebfjkwe@fjwehfj.com','(32) 73732-7327','jwebfhfwefhwefw','win10_serial.txt','127.0.0.1','2021-05-18 21:26:55'),(57,'jifnwejfbwejfb','kwjebfjkwe@fjwehfj.com','(32) 73732-7327','jwebfhfwefhwefw','win10_serial.txt','127.0.0.1','2021-05-18 21:27:25'),(58,'fknfjkwnejkfwen','kjfwenjf@fkwenkfe.com','(23) 73273-2737','weewuiuiweweuiewuiwuieuiwe','win10_serial.txt','127.0.0.1','2021-05-18 21:30:00'),(59,'skcnsdjkv','jwenfw@dnfdn.comm','(23) 23333-3232','mfw fmnwefknwejfknwejkfnwe','win10_serial.txt','127.0.0.1','2021-05-19 14:13:20'),(60,'TESTE MAIL','contato@tfarte.com.br','(32) 23421-3214','weuifhweihfjw','win10_serial.txt','127.0.0.1','2021-05-19 14:14:45'),(61,'teste','contato@tfarte.com.br','(23) 24423-4324','fwejifnwejifwejif','win10_serial.txt','127.0.0.1','2021-05-19 14:20:20'),(62,'teste attach','contato@tfarte.com.br','(21) 92333-7823','jkdbwjkebfwjkebfjk','win10_serial.txt','127.0.0.1','2021-05-19 14:21:04'),(63,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:29:44'),(64,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:29:59'),(65,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:30:35'),(66,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:30:43'),(67,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:30:51'),(68,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:30:59'),(69,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:31:03'),(70,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:31:36'),(71,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:31:41'),(72,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:31:55'),(73,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:32:04'),(74,'ewuieuiweui','contato@tfarte.com.br','(23) 3232-3232','dwjdwnfjweibfjwn','win10_serial.txt','127.0.0.1','2021-05-19 14:32:26'),(75,'oqwiioioqqwioqwio','contato@tfarte.com.br','(38) 23732-7373','wjeofnwejofnwejfnwe','win10_serial.txt','127.0.0.1','2021-05-19 14:33:29'),(76,'qwjfqwbfjwbjk','contato@tfarte.com.br','(32) 89282-3783','jwenfjkwenfjkwenfjkwen','win10_serial.txt','127.0.0.1','2021-05-19 14:35:22'),(77,'euiweuieiwui','contato@tfarte.com.br','(32) 32732-7827','jfwenfjkwenfjkwenjk','win10_serial.txt','127.0.0.1','2021-05-19 14:42:42');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-19 11:51:49
